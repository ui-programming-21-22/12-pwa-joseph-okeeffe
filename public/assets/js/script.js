const canvas = document.querySelector("#theCanvas");
const context = canvas.getContext("2d");

window.addEventListener("keydown", input);
window.addEventListener("keyup", input);
window.addEventListener("scroll", input);
window.addEventListener("mousedown", input);
window.addEventListener("mouseup", input);
window.addEventListener("touchstart", input);


let smush = new Audio('assets/sounds/smush.wav');
let gameOver = new Audio('assets/sounds/gameOver.wav');
gameOver.volume = 0.5;
smush.volume = 0.5;


let clockText = document.getElementById("clock");

// IMAGES
let dinoImage = new Image();
let manImage = new Image();
let clockImage = new Image();

let playerInput = new PlayerInput("None");

let playerPosX = 200;
let playerPosY = 200;

let dinoPosX = 1200;
let dinoPosY = 280;
let dinoSizeX = 400;
let dinoSizeY = 700;

let clockPosX = 100;
let clockPosY = 100;
let clockWidth = 50;
let clockHeight = 75;

let run = false;

let score = 0;
let scoreText = document.getElementById("score").innerHTML = "Score: " + score;
document.getElementById("score").style.fontSize = "20px";
document.getElementById("score").style.fontFamily = "Verdana";


let sprintText = document.getElementById("sprint").innerHTML = "Sprinting: Disabled ";
document.getElementById("sprint").style.fontSize = "20px";
document.getElementById("sprint").style.fontFamily = "Verdana";



let dino = new GameObject(dinoImage, dinoPosX, dinoPosY, dinoSizeX, dinoSizeY);

let player = new GameObject(manImage, playerPosX, playerPosY, 50, 50);

let clock = new GameObject(clockImage, clockPosX, clockPosY, clockWidth, clockHeight);

let currentImage = 0; // maxTime 8
let direction = 2; // maxTime 3

let currentLoopIndex = 0;
let frameCount = 0;
const walkLoop = [0, 1, 2, 3, 4, 5, 6, 7, 8];
let frameLimit = 5;

function GameObject(spritesheet, x, y, width, height) 
{
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
 
}

function imageLoader()
{  
    dinoImage.src = "assets/images/dino.png"
    manImage.src = "assets/images/spritesheet.png"
    clockImage.src = "assets/images/clock.png"
}

imageLoader();

function PlayerInput(input) 
{
    this.action = input; 
}

function gameLoop()
{
    update();
    draw();
    window.requestAnimationFrame(gameLoop);
}
window.requestAnimationFrame(gameLoop);

function draw()
{
    context.clearRect(0, 0, canvas.width, canvas.height);

    context.drawImage(dinoImage, 0, 0, 494, 1060, dinoPosX, dinoPosY, dinoSizeX, dinoSizeY);

    drawHealthbar();
    manageClock();
    animate();
   

}

const scale = 2;
const width = 64;
const height = 64;
const scaledWidth = width * scale;
const scaledHeight = height * scale;

function drawFrame(frameX, frameY, canvasX, canvasY)
{
    context.drawImage(manImage,
        frameX * width, 
        frameY * height, 
        width, 
        height,
        canvasX, 
        canvasY, 
        scaledWidth, 
        scaledHeight);
}


function animate()
{
    if (playerInput.action != "None")
    {
        frameCount++;

        if (frameCount >= frameLimit) 
        {
            frameCount = 0;
            currentLoopIndex++;

            if (currentLoopIndex >= walkLoop.length) 
            {
                currentLoopIndex = 0;
            }
        }      
    }
    else
    {
        currentLoopIndex = 0;
    }
    
    drawFrame(walkLoop[currentLoopIndex], direction, player.x, player.y);
}

function update()
{
   move();
  // checkVisbility();
       
}


function input(event)
{
    // MOVEMENT
    if (event.type === "keydown") 
    {
        switch (event.keyCode)
        {
            case 37: // Left Arrow
                playerInput = new PlayerInput("Left");
                break;
            case 38: // Up Arrow
                playerInput = new PlayerInput("Up");
                break; 
            case 39://Right Arrow
                playerInput = new PlayerInput("Right");
                break; 
            case 40: // Down Arrow
                playerInput = new PlayerInput("Down");
                break;
            case 32: // Space Bar
                playerInput = new PlayerInput("Space");
                if(run === false)
                {
                    run = true;
                }
                else
                {
                    run = false;
                }
               
                break; 
            default:
                playerInput = new PlayerInput("None"); //No Input
        }
    } 
 
    else if (event.type === "mousedown" || event.type === "touchstart")
    {
        if(event.target.className === "button left round")
        {
            playerInput = new PlayerInput("Left");
        }
        else if ( event.target.className === "button up round")
        {
            playerInput = new PlayerInput("Up");
        }
        else if ( event.target.className === "button right round")
        {
            playerInput = new PlayerInput("Right");
        }
        else if ( event.target.className === "button down round")
        {
            playerInput = new PlayerInput("Down");
        }
    }

    else
    {
        playerInput = new PlayerInput("None"); //No Input
    }
}

let dynamic = nipplejs.create({
        zone: document.getElementById('joystick'),
        color: 'blue',
});

dynamic.on('added', function (event, joyStick) 
{
    console.log("Created joystick");

    joyStick.on('dir:up', function(event, data)
    {
        console.log("UP");
        playerInput = new PlayerInput("Up");

    });

    joyStick.on('dir:down', function(event, data)
    {
        console.log("Down");
        playerInput = new PlayerInput("Down");

    });

    joyStick.on('dir:left', function(event, data)
    {
        console.log("Left");
        playerInput = new PlayerInput("Left");

    });

    joyStick.on('dir:right', function(event, data)
    {
        console.log("RIGHT");
        playerInput = new PlayerInput("Right");

    });

    joyStick.on('end', function (event, data)
    {
        playerInput = new PlayerInput("None");
    });

});

let speed = 5;
function move()
{
    if(playerInput.action === "Up")
    {
        if(player.y > -25)
        {
            player.y -= speed;
        }
       
        direction = 0;
    }

    if(playerInput.action === "Down")
    {
        if(player.y < 370)
        {
            player.y += speed
        }
        
        direction = 2;
    }

    if(playerInput.action === "Left")
    {
        if(player.x > -35)
        {
            player.x -= speed
        }
        direction = 1;
    }
    
    if(playerInput.action === "Right")
    {
        if(player.x < 1710)
        {
            player.x += speed
        }
        
        direction = 3;
    }

    if(run === true)
    {
        speed = 10
        sprintText = document.getElementById("sprint").innerHTML = "Sprinting: Enabled ";
        frameLimit = 3.5;
       
    }
    else
    {
        speed = 5;
        sprintText = document.getElementById("sprint").innerHTML = "Sprinting: Disabled ";
        frameLimit = 5;
    }
}

// CLOCK

function randoPos(rangeX, rangeY, delta)
{
    this.x = Math.abs(Math.floor(Math.random() * rangeX) - delta);
    this.y = Math.abs(Math.floor(Math.random() * rangeY) - delta);
}

let clockPosition = new randoPos(1099, 499, 180);


function manageClock()
{
    context.drawImage(clockImage, clockPosition.x, clockPosition.y, clockWidth, clockHeight);
    clock.x = clockPosition.x;
    clock.y = clockPosition.y;

    if (player.x < clock.x + clock.width && // Left Side of player x Right side of clock
        player.x + player.width * scale > clock.x && 
        player.y < clock.y &&
        player.y + player.height * scale > clock.y ) 
    {
        clockPosition = new randoPos(1700, 600, 200);
        if(currentTime < maxTime)
        {
            currentTime += 10;
            score++;
            localStorage.setItem("score", score);
        }
        
        
        smush.play();      
       
    }  
    scoreText = document.getElementById("score").innerHTML = "Score: " + score; 
    
}

const maxTime = 100;
let currentTime = 100;

function drawHealthbar() 
{
    let width = 100;
    let height = 20;
  
    // Draw the background
    context.fillStyle = "Black";
    //context.clearRect(0, 0, canvas.width, canvas.height);
    context.fillRect(player.x + 10, player.y, width, height);
  
    // Draw the fill
    context.fillStyle = "Magenta";
  
    let fillcurrentTime = Math.min(Math.max(currentTime / maxTime, 0), 1);
    context.fillRect(player.x + 10, player.y, fillcurrentTime * width, height);
    
   
  }
  maxTime

  function decreaseTime()
  {
    currentTime -= 10;
  }

  setInterval(decreaseTime, 1000);




let gameVisibility = document.getElementById("game");
let formVisibility = document.getElementById("form");
let modalVisibility = document.getElementById("modal");

function checkVisibility()
{
    event.preventDefault();
    //let queryString = document.location.search;
    //var name = queryString.split("=")[1];
    let name = document.forms["details"]["name"].value;
    if (name == undefined)
    {
        alert("Please Input a Username");
    }
    else
    {
        localStorage.setItem("username", name);
        gameVisibility.style.visibility = "visible";
        formVisibility.style.visibility = "hidden";
        //modal.style.visibility = "hidden";
    }
  
}


if(typeof(Storage) !== "undefined") 
{
    const username = localStorage.getItem('username');
     score = localStorage.getItem('score');
    console.log(score)
    if (username)
    {
        console.log("found saved username: " + username);
        let form = document.forms["details"];
        form.style.display = "none";
        let modalContent = modal.children[0].children[2];
        modalVisibility.style.display = "block";
        modalContent.innerHTML = "username: " + username + "<br>" + "score: " + score;
        let header = document.getElementById("main-header");
        header.innerHTML = "Hello " + username;
        let validateButton = document.getElementsByClassName("saved-data-accept")[0];
        let dismissButton = document.getElementsByClassName("saved-data-refusal")[0];
        validateButton.onclick = function()
        {
            modalVisibility.style.display = "none";
            gameVisibility.style.visibility = "visible";
        }
     
        dismissButton.onclick = function()
        {
            localStorage.clear();
            modalVisibility.style.display = "none";
            form.style.display = "block";
            score = 0;
             scoreText = document.getElementById("score").innerHTML = "Score: " + score; 
        }
    }
    else
    {
        console.log("no data in localStorage, loading new session");
        
    }
  } 
else 
{
    console.log("Local storage is not supported.");
}



  

 
